;; SPDX-License-Identifier: GPL-3.0-or-later
;; SPDX-FileCopyrightText: © 2022-2023 Philip McGrath <philip@philipmcgrath.com>

(define-module (wip-guix build-system racket)
  #:use-module (guix build-system)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix import utils)
                #:select (snake-case))
  #:use-module (guix derivations)
  #:use-module (guix monads)
  #:use-module (guix packages)
  #:use-module (guix search-paths)
  #:use-module (guix store)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:export (racket->package-name
            guix-package->racket-name
            infer-racket-package-name
            #;converted-catalog-details
            #;extracted-package-metadata
            racket-package-from-cycle
            make-racket-installation
            %racket-build-system-modules
            %racket-default-modules
            racket-build
            racket-build-system))

;;; Commentary:
;;;
;;; ...
;;;
;;; Code:
;;;

(define (racket->package-name name)
  "Given the NAME of a Racket package, return a Guix-style package name."
  ;; A Racket package name uses a-z, A-Z, 0-9, _, and -
  (let ((converted (snake-case name)))
    (if (string-prefix? "racket-" converted)
        converted
        (string-append "racket-" converted))))

(define (guix-package->racket-name package)
  "Given a Racket PACKAGE, return the possibly-inferred upstream name, or #f if
the upstream name is not specified and can't be inferred."
  (or (assoc-ref (package-properties package) 'upstream-name)
      (infer-racket-package-name (package-name package))))

(define (infer-racket-package-name guix-name)
  "Given the GUIX-NAME of a Racket package, return the inferred upstream name,
or #f if it can't be inferred.  If the result is not #f, supplying it to
'racket->package-name' would produce GUIX-NAME.

See also 'guix-package->racket-name', which respects the 'upstream-name'
property."
  (and (string-prefix? "racket-" guix-name)
       (substring guix-name (string-length "racket-"))))

(define %racket-build-system-modules
  ;; Build-side modules imported by default.
  `((wip-guix build racket-build-system)
    (guix build union)
    ,@%gnu-build-system-modules))

(define %racket-default-modules
  ;; Modules in scope in the build-side environment.
  '((wip-guix build racket-build-system)
    (guix build union)
    (guix build utils)))

(define (default-racket-vm)
  "Return the default Racket VM package for builds."
  ;; Lazily resolve the binding to avoid a circular dependency.
  (let ((racket (resolve-interface '(gnu packages racket))))
    (module-ref racket 'racket-vm-cs)))

(define (default-raco-test-catalog)
  "Return the catalog used for dependencies of 'raco test' in builds."
  ;; Lazily resolve the binding to avoid a circular dependency.
  (let ((racket (resolve-interface '(wip-gnu packages racket))))
    (module-ref racket 'raco-test-catalog)))

(define (make-bootstrap-guix-racket-build-lib racket-vm)
  "Return our helper scripts for building with RACKET-VM, lazily avoiding a
dependency cycle."
  ;; Lazily resolve the binding to avoid a circular dependency.
  (let* ((racket (resolve-interface '(wip-gnu packages racket)))
         (make-bootstrap-guix-racket-build-lib
          (module-ref racket 'make-bootstrap-guix-racket-build-lib)))
    (make-bootstrap-guix-racket-build-lib racket-vm)))


(define* (forbid-keyword-arguments keywords args #:optional failure-proc)
  (let loop ((rest args))
    (match rest
      (()
       args)
      ((kw _ rest ...)
       (cond
        ((not (memq kw keywords))
         (loop rest))
        (failure-proc
         (failure-proc kw))
        (else
         (error "forbidden")))))))

(define* (lower-package name
                        #:key source
                        (racket-vm (default-racket-vm))
                        path
                        (cyclic-sources '())
                        #:allow-other-keys
                        #:rest arguments)
  ""
  ;; TODO: implicit "base"
  (define (parse-plan-entry-tail base tail)
    (define-values (maybe-dir maybe-name)
      (match tail
        (()
         (values #f #f))
        ((maybe-dir)
         (values maybe-dir #f))
        ((maybe-dir name)
         (values maybe-dir name))))
    (define base+dir
      (if maybe-dir
          (file-append base (string-append "/" maybe-dir))
          base))
    (if maybe-name
        #~(#$base+dir #$maybe-name)
        base+dir))
  (define parse-cyclic-source
    (match-lambda
      ;; TODO: 'multi-clone, 'source, general gexp, other stuff?
      (('racket . (and tail (or ((? string?))
                                ((? string?) (? string?)))))
       (parse-plan-entry-tail (package-source racket-vm) tail))))
  (define path*
    (match path
      ((or #f
           (? string?)
           ((or #f (? string?)))
           ((or #f (? string?)) (? string?)))
       (if source
           (parse-plan-entry-tail source
                                  (if (list? path)
                                      path
                                      (list path)))
           (error "missing source")))
      ;; TODO: multi-clone
      (('racket . (and tail (or ((? string?))
                                ((? string?) (? string?)))))
       (parse-plan-entry-tail (package-source racket-vm) tail))))
  (apply lower-package-or-cycle-member
         'package
         #~(#:unpack-plan '(#$path*
                            #$@(map parse-cyclic-source cyclic-sources)))
         name
         (default-keyword-arguments
           (ensure-keyword-arguments
            (strip-keyword-arguments `(#:path #:cyclic-sources)
                                     arguments)
            `(#:racket-vm ,racket-vm))
           `(#:tests? #t))))

(define* (lower-cycle-member name
                             #:key cycle-member
                             #:allow-other-keys
                             #:rest arguments)
  (unless (string? cycle-member)
    (error "missing #:cycle-member"))
  (apply lower-package-or-cycle-member
         'cycle-member
         #~(#:cycle-member '#$cycle-member)
         name
         (default-keyword-arguments
           (forbid-keyword-arguments
            `(#:foreign-libs #:lib-search-dirs)
            (strip-keyword-arguments `(#:cycle-member) arguments))
           (list #:tests? #f
                 #:phases #~%cycle-member-phase))))

(define* (lower-package-or-cycle-member
          build-type generated-kw-args name
          #:key
          (racket-vm (default-racket-vm))
          (build-lib (make-bootstrap-guix-racket-build-lib racket-vm))
          (tests? #t)
          (raco-test-catalog (match tests?
                               ((or #f 'this-package-provides-raco-test)
                                #f)
                               (_
                                (default-raco-test-catalog))))
          #:allow-other-keys
          #:rest arguments)
  (apply lower-common
         build-type
         generated-kw-args
         name
         (substitute-keyword-arguments
             (ensure-keyword-arguments
              (strip-keyword-arguments `(#:racket-vm #:raco-test-catalog)
                                       arguments)
              `(#:tests? ,tests? #:build-lib ,build-lib))
           ((#:native-inputs native-inputs '())
            `(("racket-vm" ,racket-vm)
              ,@(if raco-test-catalog
                    `(("raco-test-catalog" ,raco-test-catalog))
                    `())
              ,@native-inputs)))))

(define* (lower-layer name
                      #:key system target
                      (racket (default-racket-vm))
                      (build-lib
                       (match (assoc-ref (bag-build-inputs
                                          (package->bag racket system target
                                                        #:graft? #f))
                                         "bootstrap-guix-racket-build-lib")
                         ((build-lib)
                          build-lib)
                         (#f
                          ;; #:racket is a vm package
                          (make-bootstrap-guix-racket-build-lib racket))))
                      (packages '())
                      (tethered? #t)
                      #:allow-other-keys
                      #:rest arguments)
  (apply lower-common
         'layer
         #~(#:tethered? '#$tethered?
            #:packages '#$packages)
         name
         (substitute-keyword-arguments
             (default-keyword-arguments
               (ensure-keyword-arguments
                (strip-keyword-arguments '(#:racket #:packages #:tethered?)
                                         arguments)
                `(#:build-lib ,build-lib))
               (list #:phases #~%installation-layer-phases))
           ((#:native-inputs native-inputs '())
            `(("racket" ,racket)
              ,@native-inputs)))))

(define* (lower-common build-type generated-kw-args name
                       #:key source inputs native-inputs outputs system target
                       build-lib ; ensured
                       foreign-libs
                       #:allow-other-keys
                       #:rest arguments)
  (let* ((arguments
          (strip-keyword-arguments
           `(#:inputs #:native-inputs #:target #:build-lib #:foreign-libs)
           arguments))
         (arguments
          (forbid-keyword-arguments '(#:build-type #:generated-kw-args)
                                    arguments))
         (arguments
          (if foreign-libs
              (substitute-keyword-arguments arguments
                ((#:lib-search-dirs dirs #f)
                 #~`(#$@(if dirs #~(,@#$dirs) #~())
                     #$@(map
                         (lambda (lib-name)
                           (match (assoc-ref inputs lib-name)
                             ((pkg)
                              (file-append pkg "/lib"))
                             (#f
                              (raise
                               (formatted-message
                                (G_ "missing input '~a' to the '~a' package")
                                lib-name
                                name)))))
                         foreign-libs))))
              arguments))
         (arguments
          (cons* #:build-type build-type
                 #:generated-kw-args generated-kw-args
                 arguments)))
    (cond
     (target
      ;; Cross-compilation is not yet supported.
      #f)
     (else
      (bag
        (name name)
        (system system)
        (host-inputs
         `(,@(if source
                 `(("source" ,source))
                 `())
           ,@inputs))
        (build-inputs
         `(("bootstrap-guix-racket-build-lib" ,build-lib)
           ,@native-inputs))
        (outputs outputs)
        (build racket-build)
        (arguments arguments))))))


(define* (racket-build name inputs
                       #:key
                       guile source
                       (outputs '("out"))
                       (search-paths '())
                       (tests? #t)
                       (parallel-build? #t)
                       (parallel-tests? #t)
                       (patch-shebangs? #t)
                       ;;(strip-binaries? #t)
                       ;;(strip-flags %strip-flags)
                       ;;(strip-directories %strip-directories)
                       (validate-runpath? #t)
                       (make-dynamic-linker-cache? #t)
                       ;;(license-file-regexp %license-file-regexp)
                       (phases '%standard-phases)
                       (locale "en_US.utf8")
                       (system (%current-system))
                       (build (nix-system->gnu-triplet system))
                       (imported-modules %racket-build-system-modules)
                       (modules %racket-default-modules)
                       (substitutable? #t)
                       allowed-references
                       disallowed-references
                       ;; --------
                       build-type ; ensured
                       generated-kw-args ; ensured
                       (lib-search-dirs #~'()))
  "Build SOURCE using RACKET."
  (define builder
    (with-imported-modules imported-modules
      #~(begin
          (use-modules #$@(sexp->gexp modules))
          #$(with-build-variables inputs outputs
              #~(racket-build #:name #$name
                              #:source #+source
                              #:system #$system
                              #:build #$build
                              #:outputs %outputs
                              #:inputs %build-inputs
                              #:search-paths
                              '#$(sexp->gexp
                                  (map search-path-specification->sexp
                                       search-paths))
                              #:phases #$(if (pair? phases)
                                             (sexp->gexp phases)
                                             phases)
                              #:locale #$locale
                              #:tests? '#$tests?
                              #:parallel-build? #$parallel-build?
                              #:parallel-tests? #$parallel-tests?
                              #:patch-shebangs? #$patch-shebangs?
                              ;;#:strip-binaries? #$strip-binaries?
                              ;;#:strip-flags #$strip-flags
                              ;;#:strip-directories #$strip-directories
                              #:validate-runpath? #$validate-runpath?
                              #:make-dynamic-linker-cache?
                              #$make-dynamic-linker-cache?
                              ;;#:license-file-regexp #$license-file-regexp
                              ;; --------
                              #:lib-search-dirs #$lib-search-dirs
                              #:build-type '#$build-type
                              #$@generated-kw-args)))))
  (mlet %store-monad ((guile (package->derivation (or guile (default-guile))
                                                  system #:graft? #f)))
    ;; Note: Always pass #:graft? #f.  Without it, ALLOWED-REFERENCES &
    ;; co. would be interpreted as referring to grafted packages.
    (gexp->derivation name builder
                      #:system system
                      #:target #f
                      #:graft? #f
                      #:substitutable? substitutable?
                      #:allowed-references allowed-references
                      #:disallowed-references disallowed-references
                      #:guile-for-build guile)))

(define racket-build-system
  (build-system
    (name 'racket)
    (description "A build system for Racket packages")
    (lower lower-package)))

(define racket-cycle-member-build-system
  (build-system
    (name 'racket-cycle-member)
    (description
     "Variant of @code{racket-build-system} for packages in dependency cycles.")
    (lower lower-cycle-member)))


(define racket-installation-build-system
  (build-system
    (name 'racket-installation)
    (description "A build system for creating Racket installation layers.")
    (lower lower-layer)))

(define (racket-package-from-cycle racket-name version cycle)
  (define guix-name
    (racket->package-name racket-name))
  (package
    (name guix-name)
    (version version)
    (properties
     (if (equal? racket-name (infer-racket-package-name guix-name))
         '()
         `((upstream-name . ,racket-name))))
    (source #f)
    (build-system racket-cycle-member-build-system)
    (inputs (map (cut list cycle <>)
                 (package-outputs cycle)))
    (arguments
     (list
      #:cycle-member racket-name
      #:tests? #f ;; will be covered as part of the cycle
      #:phases #~%cycle-member-phases))
    (home-page
     (string-append "https://pkgs.racket-lang.org/package/" racket-name))
    (synopsis #f)
    (description #f)
    (license #f)))

(define default-racket-installation-packages
  (delay
    (list (let ((racket (resolve-interface '(wip-gnu packages racket))))
            (module-ref racket 'racket-lib)))))

(define* (make-racket-installation #:key
                                   (racket (default-racket-vm))
                                   (name "racket-installation")
                                   (version (package-version racket))
                                   (tethered? #t)
                                   (packages
                                    default-racket-installation-packages))
  ""
  (let ((name* name)
        (version* version))
    (package
      (name name*)
      (version version*)
      (source #f)
      (build-system racket-installation-build-system)
      (inputs
       (cons racket
             (force packages)))
      (arguments
       (list
        #:phases #~%installation-layer-phases
        #:packages (map guix-package->racket-name
                        (force packages))
        #:racket racket
        #:tethered? tethered?))
      (home-page "https://racket-lang.org")
      (synopsis #f)
      (description #f)
      (license #f))))
