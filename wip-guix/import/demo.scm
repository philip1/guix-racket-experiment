;; SPDX-License-Identifier: GPL-3.0-or-later
;; SPDX-FileCopyrightText: © 2023 Philip McGrath <philip@philipmcgrath.com>
;; SPDX-FileCopyrightText: guix/scripts/import.scm contributors

(define-module (wip-guix import demo)
  #:use-module (wip-guix import racket)
  #:use-module (guix ui)
  #:use-module (guix read-print)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:export (guix-import-racket
            import))

(define print-result
  (let ((print (lambda (expr)
                 (pretty-print-with-comments (current-output-port) expr))))
    (match-lambda
      ((and expr (or ('package _ ...)
                     ('let _ ...)
                     ('define-public _ ...)))
       (print expr))
      ((? list? expressions)
       (for-each (lambda (expr)
                   (print expr)
                   ;; Two newlines: one after the closing paren, and
                   ;; one to leave a blank line.
                   (newline) (newline))
                 expressions))
      (x
       (leave (G_ "'~a' import failed~%") "demo")))))

(define* (guix-import-racket name #:key recursive?)
  (print-result
   (if recursive?
       (map (match-lambda
              ((and ('package ('name name) . rest) pkg)
               `(define-public ,(string->symbol name)
                  ,pkg))
              (_ #f))
            (racket-recursive-import name))
       ;; Single import
       (let ((sexp (racket->guix-package name)))
         (unless sexp
           (leave (G_ "failed to download meta-data for package '~a'~%")
                  name))
         sexp))))

(define import guix-import-racket)
