;; SPDX-License-Identifier: GPL-3.0-or-later
;; SPDX-FileCopyrightText: © 2022-2023 Philip McGrath <philip@philipmcgrath.com>

(define-module (wip-guix import racket)
  #:use-module (ice-9 exceptions)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 vlist)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-34)
  #:use-module ((srfi srfi-35) #:renamer (lambda (s)
                                           (case s
                                             ((&error) 'srfi-35:&error)
                                             ((error?) 'srfi-35:error?)
                                             (else s))))
  #:use-module (guix utils)
  #:use-module (guix base16)
  #:use-module (guix base32)
  #:use-module (guix hash)
  #:use-module (guix http-client)
  #:use-module (guix memoization)
  #:use-module (guix diagnostics)
  #:use-module (guix derivations)
  #:use-module (guix i18n)
  #:use-module ((guix ui) #:select (display-hint))
  #:use-module ((guix build utils)
                #:select ((package-name->name+version
                           . hyphen-package-name->name+version)
                          find-files
                          invoke))
  #:use-module ((guix import utils)
                #:select (factorize-uri

                          flatten

                          ;; NOT url-fetch : conflicts with (guix download)
                          guix-hash-url

                          package-names->package-inputs
                          maybe-inputs
                          maybe-native-inputs
                          maybe-propagated-inputs
                          package->definition

                          spdx-string->license
                          license->symbol

                          snake-case
                          beautify-description
                          beautify-synopsis

                          alist->package

                          read-lines
                          chunk-lines

                          guix-name

                          recursive-import))
  #:use-module (guix monads)
  #:use-module (guix store)
  #:use-module (guix git)
  #:use-module (guix import json)
  #:use-module (guix records)
  #:autoload   (gcrypt hash) (hash-algorithm sha1) ;; NOT sha256:
  ;; it seems to cause sha256 in the origin initializer to be unrecognized.
  ;; Rely on sha256 being the default hash algorithm.
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (json)
  #:use-module (guix packages)
  #:use-module (guix upstream)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (wip-guix build-system racket)
  #:use-module ((gnu packages racket)
                #:select (racket-vm-cs))
  #:use-module ((wip-gnu packages racket)
                #:select (%racket-version
                          racket-release-origin
                          bootstrap-guix-racket-import-lib))
  #:export (racket-recursive-import
            current-catalog
            racket->guix-package))

;;; COMMENTARY:
;;;
;;; The Racket package catalog protocol is documented at:
;;;
;;;     https://docs.racket-lang.org/pkg/catalog-protocol.html
;;;
;;; It uses Racket hash table literals, which are not supported by Guile's
;;; reader. Furthermore, the hash tables may contain additional concrete
;;; syntax not supported by Guile's reader (at least regular expression
;;; literals), and some metadata we want is not required in the catalog
;;; entries but may require evaluating `#lang info` modules.
;;;
;;; All of this could be implemented in Guile in principle. Alternatively, we
;;; could just run Racket, which would certainly be the easiest approach.
;;;
;;; For now, we instead rely on the undocumented JSON serialization used
;;; internally by the main package catalog at <https://pkgs.racket-lang.org>
;;; to communicate between the back-end from
;;; <https://github.com/racket/pkg-index> and the front-end from
;;; <https://github.com/racket/racket-pkg-website>. We trade the ability to
;;; support arbitrary catalogs for the ability to implement this similarly to
;;; other importers.
;;;
;;; FIXME: Apparently 'version is not in the catalog, so we need to download
;;; the packages and check their info.rkt files.
;;;
;;; CODE:
;;;

(define %keep-going?
  (make-parameter #f (cut and <> #t)))

(define %cycle-representatives
  '("drracket"
    "functional-doc"
    "functional-lib"
    "graphics"
    "graphite-doc"
    "htdp-lib"
    "lens-common"
    "racket-doc"
    "racket-lib"
    "racket-test"
    "stxparse-info"
    "typed-peg"))

(define %catalog-url
  "https://pkgs.racket-lang.org")

(define %check-version-url
  "https://download.racket-lang.org/version.txt")

#|
user chooses:
    catalog to use
    tag of release packages (even if we should use latest)
    packages in release


|#

(define-syntax condition*
  (syntax-rules ()
    ((_ base)
     base)
    ((_ base (type field-spec ...) ...)
     (make-compound-condition base
                              (condition (type field-spec ...) ...)))))

(define (raise-field-error who field value)
  (raise (condition*
          (formatted-message (G_ "~a: bad value for ~a: ~s")
                             who field value))
         (&assertion-failute)))
(define-syntax check-match
  (syntax-rules (quote)
    ((_ pat 'who 'field)
     (lambda (x)
       (match x
         (pat x)
         (_ (raise-field-error 'who 'field x)))))))

(define-record-type* <catalog-pkg> catalog-pkg make-catalog-pkg
  catalog-pkg?
  (name catalog-pkg-name
        (sanitize (check-match (? string?) 'catalog-pkg 'name)))
  (checksum catalog-pkg-checksum
            (sanitize
             (check-match (or #f (? string?)) 'catalog-pkg 'checksum)))
  (dependencies catalog-pkg-dependencies
                ;; using ___ instead of ... here avoids what appears to be
                ;; a bug in define-record-type*
                (sanitize (check-match
                           (or (('cycle-member . (? string?)))
                               (('cyclic-sources (? string?) ___)
                                ('inputs (? string?) ___)))
                             'catalog-pkg 'dependencies)))
  (source catalog-pkg-source
          (sanitize (check-match
                     (or (? exception?)
                         (? string?)
                         (('path . (or #f (? string?)))
                          ('rev . (or 'head (? string?)))
                          ('url . (? string?))))
                       'catalog-pkg 'source))))

(define-record-type* <resolved-pkg> resolved-pkg make-resolved-pkg
  resolved-pkg?
  (catalog-pkg resolved-pkg-catalog-pkg
               (sanitize (check-match
                          (or (? catalog-pkg? v)
                              (? exception? v))
                          'resolved-pkg 'catalog-pkg)))
  (quoted-origin resolved-pkg-quoted-origin
                 (delayed))
  (info resolved-pkg-info
        (delayed)
        (sanitize (check-match
                   (('license . _)
                    ('pkg-desc . (? string?))
                    ('version . (? string?)))
                   'resolved-pkg 'info))))

(define-record-type* <catalog> racket-catalog make-catalog-record
  catalog?
  (source catalog-source (sanitize (check-match
                                    (? string?)
                                    'catalog 'source)))
  (resolved-packages catalog-resolved-packages
                     (delayed))) ; (vhash/c string? resolved-package?)

(define-condition-type &catalog-data-error &error
  catalog-data-error?)
(define-condition-type &recoverable &error
  recoverable-error?
  (recovered-value exception-recovered-value))

(define valid-version?
  ;; Ported from "<collects>/version/utils.rkt".
  ;; Dual-licensed under Apache-2.0 or MIT/Expat, at your option.
  (letrec*
      (;; Sadly, Guile doesn't like this.
       ;; (It works with Racket's `pregexp`.)
       #;
       (rx:version (make-regexp
                    (string-append
                     "^(0|[1-9][0-9]*)[.]"
                     "(0|(0|[1-9][0-9]{0,1})([.](0|[1-9][0-9]{0,2})){0,2}"
                     "(?<![.]0))$")))
       #;
       (valid-version? (lambda (s)
                         (and (string? s) (regexp-exec rx:version s) #t)))
       (1..9  (char-set #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9))
       (1..9? (cut char-set-contains? 1..9 <>))
       (0..9  (char-set-adjoin 1..9 #\0))
       (0..9? (cut char-set-contains? 0..9 <>))
       (major (match-lambda
                ((#\0 #\. . rst)
                 (minor rst))
                (((? 1..9?) . rst)
                 (let loop ((rst rst))
                   (match rst
                     (((? 0..9?) . rst)
                      (loop rst))
                     ((#\. . rst)
                      (minor rst))
                     (_ #f))))
                (_ #f)))
       (minor (match-lambda
                ((#\0)
                 #t)
                ((#\0 . rst)
                 (patch rst))
                (((? 1..9?) . rst)
                 (match rst
                   (((? 0..9?) . rst)
                    (patch rst))
                   (rst
                    (patch rst))))
                (_ #f)))
       (patch (lambda (rst)
                (patch-or-dev rst dev)))
       (dev   (lambda (rst)
                (patch-or-dev rst null?)))
       (patch-or-dev (lambda (rst k)
                       (match rst
                         (()
                          #t)
                         ((#\. #\0)
                          #f)
                         ((#\. #\0 . rst)
                          (k rst))
                         ((#\. (? 1..9?) . rst)
                          (let loop ((rst rst)
                                     (n 0))
                            (and (<= n 2)
                                 (match rst
                                   (((? 0..9?) . rst)
                                    (loop rst (+ 1 n)))
                                   (rst
                                    (k rst))))))
                         (_ #f))))
       (valid-version? (lambda (v)
                         (and (string? v) (major (string->list v))))))
    valid-version?))

(define* (check-version)
  ;; Ported from "<collects>/version/check.rkt" by Racket contributors.
  ;; Dual-licensed under Apache-2.0 or MIT/Expat, at your option.
  ;; Simplified because `alpha` field is no longer relevant: see
  ;; <https://racket.discourse.group/t/racket-v8-7-release-thread/1343/10>.
  (define version-info
    (call-with-input-url/cached %check-version-url
      read))
  (unless (and (list? version-info)
               (every (λ (x) (and (list? x)
                                  (= 2 (length x))
                                  (symbol? (car x))
                                  (string? (cadr x))))
                      version-info))
    (raise (condition*
            (formatted-message
             (G_ "bad response from server: `~s'") version-info)
            (&external-error))))
  (match (assq 'stable version-info)
    ((_ (? valid-version? ver))
     ver)
    (#f
     (raise (condition*
             (formatted-message (G_ "no `~s' in response `~s'")
                                key version-info)
             (&external-error))))
    ((_ ver)
     (raise (condition*
             (formatted-message
              (G_ "bad version string ~s from server in `~s'")
              ver version-info))
            (&external-error)))))



(define* (make-catalog #:key relative-sources?
                       catalog
                       pkgs-all
                       (cycle-representatives %cycle-representatives)
                       promised-catalog-alist
                       (check-version check-version)
                       release-catalog
                       promised-release-packages)
  (define catalog-for-errors
    (or catalog pkgs-all (and (not promised-catalog-alist) %catalog-url)))
  (define promised-catalog-vh
    (delay
      (catalog-alist->vhash
       (if promised-catalog-alist
           (force promised-catalog-alist)
           (catalog-fetch #:relative-sources? relative-sources?
                          #:catalog catalog
                          #:pkgs-all pkgs-all
                          #:cycle-representatives cycle-representatives))
       #:catalog catalog-for-errors)))
  (define promised-version
    (delay
      (if (string? check-version)
          check-version
          (check-version))))
  (define promised-release-vh
    (delay
      (build-release-repository-vhash
       (force promised-catalog-vh)
       #:release-packages
       (if promised-release-packages
           (force promised-release-packages)
           (fetch-release-package-names #:version (force promised-version)
                                        #:release-catalog release-catalog)))))
  (racket-catalog
   (source catalog-for-errors)
   (resolved-packages
    (resolve-catalog-pkgs (force promised-catalog-vh)
                          #:release-vhash (force promised-release-vh)))))

(define* (catalog-fetch #:key relative-sources? catalog pkgs-all
                        (cycle-representatives %cycle-representatives))
  (define relative-sources-catalog
    (and relative-sources?
         (or catalog
             (cond
              (pkgs-all
               (raise
                (condition*
                 (formatted-message
                  (G_ "can't find sources relative to file '~a'")
                  pkgs-all)
                 (&fix-hint (hint (G_ "To resolve relative package sources,
you must supply a catalog, not merely a @file{pkgs-all} file."))))))
              (else
               %catalog-url)))))
  (call-with-input-file (convert-catalog-details
                         (raw-catalog-fetch catalog #:pkgs-all pkgs-all)
                         #:catalog relative-sources-catalog
                         #:cycle-representatives cycle-representatives)
    read))

(define* (catalog-alist->vhash alist #:key catalog)
  (when (eq? alist 'no-dependencies-in-catalog)
    (raise
     (condition*
      (if catalog
          (formatted-message
           (G_ "package catalog '~a' does not record dependencies")
           catalog)
          (formatted-message
           (G_ "selected package catalog does not record dependencies")))
      (&catalog-data-error)
      (&implementation-restriction)
      (&fix-hint
       (hint (G_ "The @code{'dependencies} entry is optional for Racket
package catalogs, but it is needed for this importer.  You may be able to use
@command{raco pkg catalog-copy} to create a catalog with the necessary
metadata."))))))
  (fold (lambda (pkg vh)
          (match pkg
            ((name . _)
             (vhash-cons name (sexp->catalog-package pkg) vh))))
        vlist-null
        alist))

(define* (resolve-catalog-pkgs catalog-vh #:key (release-vhash vlist-null))
  (let* ((hash-file-pkg-source (memoize hash-file-pkg-source))
         (resolve-git-pkg-source (memoize resolve-git-pkg-source)))
    (vhash-fold (lambda (name pkg vh)
                  (vhash-cons name
                              (catalog-pkg->resolved-pkg
                               pkg
                               #:release-vhash release-vhash
                               #:resolve-git-pkg-source resolve-git-pkg-source
                               #:hash-file-pkg-source hash-file-pkg-source)
                              vh))
                vlist-null
                catalog-vh)))


(define sexp->catalog-package
  (match-lambda
    #;"Returns a 'catalog-pkg?' for the given S-expression.  If the S-expression
    represents a broken package, returns a 'condition?', instead."
    (((? string? name)
      ('checksum . checksum)
      ('dependencies . dependencies)
      ('source . source))
     (let retry ((checksum checksum)
                 (dependencies dependencies)
                 (source source))
       (match (list checksum dependencies)
         (((('error . msg)) _)
          (condition*
           (formatted-message (G_ "checksum error"))
           (&catalog-data-error)
           (&external-error)
           (&recoverable (recovered-value (retry #f dependencies source)))))
         ((_ (('error . msg)))
          (condition*
           (formatted-message (G_ "dependencies error"))
           (&catalog-data-error)
           (&external-error)
           (&recoverable (recovered-value (retry checksum
                                                 '((cyclic-sources)
                                                   (inputs))
                                                 source)))))
         ((_ _)
          (catalog-pkg
           (name name)
           (checksum checksum)
           (dependencies dependencies)
           (source
            (match source
              ('(missing)
               (condition*
                (formatted-message
                 (G_ "missing definition for source in catalog"))
                (&catalog-data-error)
                (&external-error)))
              (('ill-formed ('message . msg) ('source . src))
               (condition*
                (formatted-message (G_ "ill-formed package source"))
                (&catalog-data-error)
                (&external-error)))
              (('relative-source-without-catalog-root
                ('source . src)
                ('type . type))
               (condition*
                (formatted-message
                 (G_ "relative w/o root (offer hints)"))
                (&catalog-data-error)))
              (('unknown-type
                ('source . src)
                ('type . type))
               (condition*
                (formatted-message
                 (G_ "unknown type"))
                (&assertion-failure)
                (&catalog-data-error)))
              (('name ('source . src))
               (condition*
                (formatted-message
                 (G_ "catalog returned an name, not a resolved source"))
                (&catalog-data-error)
                (&external-error)))
              (('dir ('source . src))
               (condition*
                (formatted-message (G_ "directory url sources not supported"))
                (&catalog-data-error)
                (&implementation-restriction)))
              ((or ('file ('source . (? string? source)))
                   ('git . (and source (('path . (or #f (? string?)))
                                        ('rev . (or 'head (? string?)))
                                        ('url . (? string?))))))
               source))))))))))

(define http/s?
  (let* ((rx (make-regexp "^https?://"))
         (http/s? (lambda (str)
                    (regexp-exec rx str))))
    http/s?))
(define (trim-http/s str)
  (and=> (http/s? str) match:suffix))

(define (file->string pth)
  (call-with-input-file pth get-string-all))

(define (call-with-input-url/cached url proc)
  (let ((in (http-fetch/cached url)))
    (dynamic-wind
      (lambda ()
        *unspecified*)
      (lambda ()
        ;; Guile's with-contuation-barrier seems to prevent error escapes ...
        (proc in))
      (lambda ()
        (close-port in)))))

(define* (raw-catalog-fetch #:optional catalog #:key pkgs-all)
  (define real-pkgs-all
    (or pkgs-all (string-append (or catalog %catalog-url) "/pkgs-all")))
  (cond
   ((if pkgs-all
        (and (string-suffix? ".sqlite" pkgs-all) pkgs-all)
        (and catalog (string-suffix? ".sqlite" catalog) catalog))
    => (lambda (sqlite)
         (raise
          (condition*
           (formatted-message
            (G_ "catalog file '~a' could not be used")
            sqlite)
           (&implementation-restriction)
           (&fix-hint (hint (G_ "Package catalogs in SQLite format are not
supported.  You may be able to use @command{raco pkg catalog-copy} to convert
the catalog to directory format.")))))))
   ((http/s? real-pkgs-all)
    (call-with-input-url/cached real-pkgs-all get-string-all))
   ((and (not (file-exists? real-pkgs-all))
         (directory-exists? (dirname real-pkgs-all)))
    (raise
     (make-compound-condition
      (formatted-message
       (G_ "file '~a' does not exist")
       real-pkgs-all)
      (&implementation-restriction)
      (&fix-hint (hint (G_ "Racket does not require that local directory
catalogs have @file{pkgs-all} files, but this importer cannot synthesize
@file{pkgs-all} files automatically.  You may be able to use @command{raco pkg
catalog-copy} to add a @file{pkgs-all} file to your catalog."))))))
   (else
    (file->string real-pkgs-all))))

(define* (convert-catalog-details data #:key catalog (cycle-representatives
                                                      %cycle-representatives))
  (with-store store
    (run-with-store store
      (mlet* %store-monad
          ((drv (converted-catalog-details data
                                           #:catalog catalog
                                           #:cycle-representatives
                                           cycle-representatives))
           (built (built-derivations (list drv))))
        (return (derivation->output-path drv))))))
(define* (converted-catalog-details pkgs-all-str
                                    #:key
                                    catalog
                                    (cycle-representatives '())
                                    (system (%current-system)))
  ""
  (define maybe-cycle-reps
    ;; Let sort signal an error if it isn't a pair.
    ;; This is probably a short list, so spend time to be deterministic.
    (if (null? cycle-representatives)
        #f
        (object->string
         (delete-duplicates
          (sort cycle-representatives string<?)))))
  (define import-lib bootstrap-guix-racket-import-lib)
  (mlet* %store-monad
      ((pkgs-all (text-file "pkgs-all" pkgs-all-str))
       (racket-vm (package->derivation racket-vm-cs system #:graft? #f))
       (import-lib (package->derivation import-lib system #:graft? #f))
       (racket-vm -> (derivation-input racket-vm '("out")))
       (import-lib -> (derivation-input import-lib '("out")))
       (vm-dir -> (string-append (derivation-input-output-path racket-vm)
                                 "/opt/racket-vm")))
    ;; Like 'git-fetch', use a fixed name so there's only one script
    ;; in the store.
    (raw-derivation "pkgs-all-converted.rktd"
                    (string-append vm-dir "/bin/racket")
                    `("--no-user-path"
                      "--config" ,(string-append vm-dir "/etc")
                      "--collects" ,(string-append vm-dir "/collects")
                      "--"
                      ,(string-append
                        (derivation-input-output-path import-lib)
                        "/guix-racket/import"
                        "/convert-catalog-details.rkt")
                      "-f" ,pkgs-all
                      "--output-env-var" "out"
                      ,@(if catalog
                            `("--catalog" ,catalog)
                            `())
                      ,@(if maybe-cycle-reps
                            `("--cycle-representatives" ,maybe-cycle-reps)
                            `()))
                    #:sources (list pkgs-all)
                    #:inputs (list racket-vm
                                   import-lib)
                    #:local-build? #t
                    #:system system)))


(define current-catalog
  (make-parameter
   '()
   (lambda (catalog-or-args)
     (if (catalog? catalog-or-args)
         catalog-or-args
         (apply make-catalog catalog-or-args)))))

(define* (make-racket-package-sexp resolved #:key (repo (current-catalog)))
  (define pkg
    (resolved-pkg-catalog-pkg resolved))
  (define name
    (catalog-pkg-name pkg))
  (define guix-name
    (racket->package-name name))
  (define-values (quoted-licenses upstream-desc upstream-version)
    (match (resolved-pkg-info resolved)
      ((('license . maybe-license-sexp)
        ('pkg-desc . upstream-desc)
        ('version . upstream-version))
       (values (cond
                (maybe-license-sexp
                 => license-sexp->license)
                (else
                 '()))
               upstream-desc
               upstream-version))))
  (define version
    ;; FIXME: use git-version and/or account for release packages
    upstream-version)
  (match (catalog-pkg-dependencies pkg)
    ((('cycle-member . (? string? rep)))
     (values
      `(package
         (inherit
          (racket-package-from-cycle
           ,name ,version
           ,(string->symbol (racket->package-name rep))))
         (synopsis ,upstream-desc) ; FIXME
         (description ,upstream-desc) ; FIXME
         (license ,quoted-licenses))
      (list rep)))
    ((('cyclic-sources cyclic-sources ...)
      ('inputs deps ...))
     (values
      `(package
         (name ,guix-name)
         (version ,version)
         (source ,(resolved-pkg-quoted-origin resolved))
         (build-system racket-build-system)
         ,@(maybe-inputs
            (map racket->package-name deps)) ;; TODO cyclic-sources
         ;; TODO: arguments
         (home-page
          ,(string-append "https://pkgs.racket-lang.org/package/" name))
         (synopsis ,upstream-desc) ; FIXME
         (description ,upstream-desc) ; FIXME
         ,@(let ((inferred-name (infer-racket-package-name guix-name)))
             (if (equal? inferred-name name)
                 `()
                 `((properties '((upstream-name . ,name))))))
         (license ,quoted-licenses))
      ;; NOT cyclic-sources here
      deps))))

(define racket->guix-package
  (let ((racket->guix-package
         (memoize
          (lambda (name version repo)
            "Is this supposed to return (values #f '()) on failure?"
            (match (vhash-assoc name (catalog-resolved-packages repo))
              ((_ . (? resolved-pkg? pkg))
               (make-racket-package-sexp pkg #:repo repo))
              ((_ . (? condition? exn))
               (raise exn))
              (#f
               ;; Should we keep a catalog name around for error reporting?
               (raise
                (formatted-message
                 (G_ "Racket package '~a' not found in catalog") name))))))))
  ;; don't memoize implicit #:repo
    (lambda* (name #:key version (repo (current-catalog))
                   #:allow-other-keys)
      (racket->guix-package name version repo))))

(define* (racket-recursive-import name
                                  #:optional version
                                  #:key (repo (current-catalog))
                                  #:allow-other-keys)
  (recursive-import name
                    #:version #f ; intentionally ignored
                    #:repo repo
                    #:repo->guix-package racket->guix-package
                    #:guix-name racket->package-name))

(define (license-sexp->license sexp)
  "Convert SEXP, a well-formed license S-expression, to a quoted license or a
list of quoted licenses."
  ;; https://docs.racket-lang.org/pkg/metadata.html#%28tech._license._s._expression%29
  (define (id->license id)
    ;; relies on + and case-folding support in spdx-string->license
    (or (spdx-string->license (symbol->string id))
        `(license:fsdg-compatible
          ,(format #f "https://spdx.org/licenses/~a.html" id)
          "Please check that this license is defined and FSDG-compatible!")))
  (define sexp->license-list
    (match-lambda
      ((? symbol? id)
       (list (id->license id)))
      (((? symbol? id) 'WITH (? symbol? exception))
       (list (id->license id)))
      ((left (or 'AND 'OR) right)
       (append (sexp->license-list left)
               (sexp->license-list right)))))
  (match (sexp->license-list sexp)
    ((license)
     license)
    ((licenses ...)
     licenses)))

(define* (fetch-release-package-names #:key version release-catalog)
  (define pkgs
    (string-append (or release-catalog
                       (string-append
                        "https://download.racket-lang.org"
                        "/releases/"
                        (or version (check-version))
                        "/catalog"))
                   "/pkgs"))
  (if (http/s? pkgs)
      (call-with-input-url/cached pkgs read)
      (call-with-input-file pkgs read)))

(define* (build-release-repository-vhash catalog-vh
                                         #:key
                                         (release-packages
                                          (fetch-release-package-names)))
  ;; With vhash-fold* we can get all of the names and thus distinguish
  ;; packages that are actually in the release from packages that just
  ;; share a Git repository with packages involved in the release.
  (fold (lambda (name vh)
          (match (vhash-assoc name catalog-vh)
            ((_ . (= catalog-pkg-source
                     (('path . _)
                      ('rev . _)
                      ('url . url))))
             (vhash-cons url name vh))
            (_
             vh)))
        vlist-null
        release-packages))

(define %release-repository-rx
  (make-regexp "^https://github.com/([^/]+)/([^/]+)$"))

(define* (resolve-pkg-source pkg release-vh
                             #:key
                             (hash-file-pkg-source hash-file-pkg-source)
                             (resolve-git-pkg-source resolve-git-pkg-source))
  (define name
    (catalog-pkg-name pkg))
  (match (catalog-pkg-source pkg)
    ((? string? uri)
     (define sha256-str
       (hash-file-pkg-source uri (catalog-pkg-checksum pkg)))
     (cons `(origin
              (method url-fetch)
              (uri ,uri)
              (sha256 (base32 ,sha256-str)))
           (origin
             (method url-fetch)
             (uri uri)
             (sha256 (base32 sha256-str)))))
    ((('path . _)
      ('rev . raw-rev)
      ('url . url))
     (define release-repo?
       (and (vhash-assoc url release-vh) #t))
     ;; FIXME: decide #:v, #:commit, or neither
     ;; need to rewrite release repositories to use correct tag before
     ;; passing them to resolve-package-source
     (match (cons release-repo?
                  (resolve-git-pkg-source url
                                          (catalog-pkg-checksum pkg)
                                          raw-rev))
       ((#t git-ref sha256-str)
        (define-values (org repo)
          (let ((m (regexp-exec %release-repository-rx
                                (git-reference-url git-ref))))
            (values (match:substring m 1) (match:substring m 2))))
        (define pre-args
          (if (equal? "racket" org)
              (list repo)
              (list org repo)))
        (define post-args
          ;; FIXME see above
          `(#:commit ,(git-reference-commit git-ref)))
        (cons `(racket-release-origin
                ,@pre-args
                (base32 ,sha256-str)
                ,@post-args)
              (apply racket-release-origin
                     `(,@pre-args
                       ,(base32 sha256-str)
                       ,@post-args))))
       ((#f git-ref sha256-str)
        (define import-file-name
          ;; NOT (git-file-name name version): we don't know what VERSION
          ;; will be until we've looked in the "info.rkt" file, and we may
          ;; share the origin for multiple packages.
          (string-append
           "racket-import-"
           (string-join (string-split (trim-http/s (git-reference-url git-ref))
                                      #\/)
                        "-")
           (let ((c (git-reference-commit git-ref)))
             (if (string-prefix? "v" c)
                 c
                 (string-take c (min 7 (string-length c)))))))
        (cons `(origin
                 (method git-fetch)
                 (uri (git-reference
                       (url ,(git-reference-url git-ref))
                       (commit ,(git-reference-commit git-ref))))
                 (sha256 (base32 ,sha256-str))
                 (file-name (git-file-name name version)))
              (origin
                (method git-fetch)
                (uri git-ref)
                (sha256 (base32 sha256-str))
                (file-name import-file-name))))))))

(define* (extract-package-metadata name origin path)
  (with-store store
    (run-with-store store
      (mlet* %store-monad
          ((drv (extracted-package-metadata name origin path))
           (built (built-derivations (list drv))))
        (return (derivation->output-path drv))))))
(define* (extracted-package-metadata name origin #:optional path
                                     #:key (system (%current-system)))
  (define import-lib bootstrap-guix-racket-import-lib)
  (mlet* %store-monad
      ((src (origin->derivation origin))
       (racket-vm (package->derivation racket-vm-cs system #:graft? #f))
       (import-lib (package->derivation import-lib system #:graft? #f))
       (src -> (derivation-input src '("out")))
       (racket-vm -> (derivation-input racket-vm '("out")))
       (import-lib -> (derivation-input import-lib '("out")))
       (vm-dir -> (string-append (derivation-input-output-path racket-vm)
                                 "/opt/racket-vm")))
    (raw-derivation (string-append (racket->package-name name)
                                   "-extracted-metadata.rktd")
                    (string-append vm-dir "/bin/racket")
                    `("--no-user-path"
                      "--config" ,(string-append vm-dir "/etc")
                      "--collects" ,(string-append vm-dir "/collects")
                      "--"
                      ,(string-append
                        (derivation-input-output-path import-lib)
                        "/guix-racket/import"
                        "/extract-metadata.rkt")
                      "--output-env-var" "out"
                      ,name
                      ,(let ((src-dir (derivation-input-output-path src)))
                         (if path
                             (string-append src-dir "/" path)
                             src-dir)))
                    #:inputs (list src
                                   racket-vm
                                   import-lib)
                    #:local-build? #t
                    #:system system)))


(define* (catalog-pkg->resolved-pkg pkg
                                    #:key
                                    (release-vhash vlist-null)
                                    (hash-file-pkg-source
                                     hash-file-pkg-source)
                                    (resolve-git-pkg-source
                                     resolve-git-pkg-source))
  (define resolved
    (delay
      (resolve-pkg-source pkg release-vhash
                          #:hash-file-pkg-source hash-file-pkg-source
                          #:resolve-git-pkg-source resolve-git-pkg-source)))
  (resolved-pkg
   (catalog-pkg pkg)
   (quoted-origin
    (match (force resolved)
      ((quoted-origin . _)
       quoted-origin)))
   (info
    (match (force resolved)
      ((_ . origin)
       (call-with-input-file
           (extract-package-metadata (catalog-pkg-name pkg)
                                     origin
                                     (match (catalog-pkg-source pkg)
                                       ((('path . path)
                                         ('rev . _)
                                         ('url . _))
                                        path)
                                       (_
                                        #f)))
         read))))))

#|
(define resolve-package-source
  (case-lambda
    "Resolves a Racket package source into two values [FIXME - a pair, to
support 'memoize]: a 'git-reference' or
string to be use as an origin uri and and the sha256 hash of its contents as a
Nix base32 string.  The first to arguments are a url string and a Racket
package checksum, or #f if no checksum is available.  For a Racket Git package
source (as opposed to a file package source), the REV must be provided as a
third argument.

This function is impure: for some combinations of arguments, the returned
values may be different depending on results from the network.  For example,
if the arguments specify that the current Git HEAD should be used, the result
will change when a new commit is pushed to the repository."
    ((url maybe-checksum rev)
     (resolve-git-pkg-source url maybe-checksum rev))
    ((url maybe-checksum)
     (resolve-file-pkg-source url maybe-checksum))))
|#
(define (hash-file-pkg-source url maybe-checksum)
  "Returns the sha256 hash of the file at URL a Nix base32 string.  If
MAYBE-CHECKSUM is not #f, it should be a hex string giving the expected sha1
hash of URL, and an exception will be raised unless the file at URL matches
the expected hash."
  ;; TODO: memoize does not handle exceptions
  (define store-pth ; should we do this for pkgs-all, too?
    (with-store store
      (download-to-store store url)))
  (define hash-str
    (bytevector->nix-base32-string
     (file-hash* store-pth ))); #:algorithm (hash-algorithm sha256))))
  (when checksum
    (let ((actual-checksum
           (bytevector->base16-string
            (file-hash* store-pth #:algorithm (hash-algorithm sha1)))))
      (unless (equal? checksum actual-checksum)
        (raise
         (condition*
          (formatted-message
           (G_ "sha1 hash ~s does not match expected ~s for file ~s \
downloaded from ~s")
           actual-checksum checksum store-pth url)
          (&external-error)
          (&recoverable (recovered-value hash-str)))))))
  hash-str)

(define (resolve-git-pkg-source url maybe-checksum rev)
  "Returns two values: a 'git-refernece' for URL and the sha256 hash of its
content as a Nix base32 string.  The commit to use is chosen based on
MAYBE-CHECKSUM, which, if it is not #f, is a Racket package checksum (and thus
a Git commit id), and on REV, the 'rev' part of a Racket Git package source,
which may be the symbol 'head or a string naming a branch, tag, or commit."
  ;; TODO: memoize does not handle exceptions
  (define typed-ref
    ;; Convert rev to a reference with explicit type as expected by
    ;; 'update-cached-checkout'.
    (cond
     ;; If rev is not 'head, it was specified in the catalog.
     ((string? rev)
      (let ((remote (remote-refs url)))
        (cond
         ;; Prefer a tag most of all.
         ((member (string-append "refs/tags/" rev) remote)
          (cons 'tag rev))
         ;; If not a tag, rev is likely a branch to follow for updates.
         ;; If the catalog reported a specific commit, prefer it, ...
         (maybe-checksum
          (cons 'commit maybe-checksum))
         ;; ... otherwise, use the head of the specified branch.
         ((member (string-append "refs/heads/" rev) remote)
          (cons 'branch ref))
         ;; If we get here, we don't understand rev.  Fall bach to 'head.
         (else
          '()))))
     ;; No explicit Git reference from catalog.
     ;; If the catalog recorded a specific commit, use it, ...
     (maybe-checksum
      (cons 'commit maybe-checksum))
     ;; ... otherwise, fall back to 'head.
     (else
      '())))
  (define-values (checkout-dir commit-oid _relation)
    (update-cached-checkout url #:ref typed-ref))
  (define hash-str
    (bytevector->nix-base32-string
     (file-hash* checkout-dir #:recursive? #t)))
  (list (git-reference
         (url url)
         (commit (match typed-ref
                   (('tag . tag)
                    tag)
                   (_
                    commit-oid))))
        hash-str))

;; Local Variables:
;; eval: (put 'call-with-input-url/cached 'scheme-indent-function 1)
;; End:
