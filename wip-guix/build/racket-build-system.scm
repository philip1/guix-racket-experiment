;; SPDX-License-Identifier: GPL-3.0-or-later
;; SPDX-FileCopyrightText: © 2022-2023 Philip McGrath <philip@philipmcgrath.com>

(define-module (wip-guix build racket-build-system)
  #:use-module ((guix build gnu-build-system) #:prefix gnu:)
  #:use-module (guix build union)
  #:use-module (guix build utils)
  #:use-module (ice-9 ftw)
  #:use-module (ice-9 match)
  #:use-module (ice-9 popen)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:export (%standard-phases
            %cycle-member-phases
            %installation-layer-phases
            racket-build))

;;; Commentary:
;;;
;;; ...
;;;
;;; <out>/lib/racket/guix/
;;;   ├── catalog/
;;;   │   └── pkg/
;;;   │       ├── base
;;;   │       ├── draw-lib
;;;   │       └── racket-lib
;;;   ├── foreign-libs/
;;;   │   ├── draw-lib
;;;   │   └── racket-lib -> /gnu/store/8whfpj7...
;;;   └── selected/
;;;       └── draw-lib
;;; <pkgs>/lib/racket/guix/
;;;   └── pkgs
;;;       └── draw-lib
;;;           ├── info.rkt
;;;           └── ...
;;;
;;; a cycle member the same shape for <out>, just pointing to the
;;; <pkgs> from its representative
;;;
;;; layer
;;;   ├── bin/
;;;   │   ├── racket
;;;   │   └── raco
;;;   ├── etc/
;;;   │   └── racket/
;;;   │       └── config.rktd
;;;   ├── lib/
;;;   │   └── racket/
;;;   │       ├── launchers.rktd
;;;   │       ├── links.rktd
;;;   │       └── pkgs/
;;;   │           └── pkgs.rktd
;;;   └── share/
;;;       ├── man/
;;;       └── racket/
;;;           └── info-cache.rktd
;;;
;;; ...
;;;
;;; Code:
;;;

(define* (set-PLTUSERHOME #:rest args)
  "Set the PLTUSERHOME environment variable to a temporary home directory for
building packages in user scope."
  (mkdir-p "pltuserhome")
  (setenv "PLTUSERHOME" (string-append (getcwd) "/pltuserhome")))

(define (PLTUSERHOME)
  (cond
   ((getenv "PLTUSERHOME"))
   (else
    (error "called before set-PLTUSERHOME"))))
(define (source-dir)
  (string-append (PLTUSERHOME) "/../source"))
(define (foreign-libs-dir)
  (string-append (PLTUSERHOME) "/foreign-libs"))
(define (inputs-catalog)
  (string-append (PLTUSERHOME) "/inputs-catalog"))
(define (pkg-names.rktd)
  (string-append (PLTUSERHOME) "/pkg-names.rktd"))
(define (all-pkg-names)
  (call-with-input-file (pkg-names.rktd) read))
(define (this-package-name)
  (car (all-pkg-names)))

(define (directory-list dir)
  "Like DIRECTORY-LIST from 'racket/base': lists the contents of DIR, not
including the special \".\" and \"..\" entries."
  (scandir dir (lambda (f)
                 (not (member f '("." ".."))))))

(define* (catalog-copy #:key racket from to merge?)
  ;; this handles rewriting relative paths
  (apply invoke
         racket "-l-"
         "raco" "pkg" "catalog-copy"
         `(,@(if merge?
                 '("--merge")
                 '())
           ,@(if (string? from)
                 (list from)
                 from)
           ,to))
  (with-directory-excursion to
    ;; these just make merging difficult
    (for-each (lambda (pth)
                (when (file-exists? pth)
                  (delete-file pth)))
              '("pkgs" "pkgs-all"))))

(define* (copy-input-catalogs #:key racket inputs
                              #:allow-other-keys)
  (catalog-copy #:racket racket
                #:to (inputs-catalog)
                #:from
                (match inputs ;; ?? native-inputs ??
                  (((_ . directory*) ...)
                   (search-path-as-list '("lib/racket/guix/catalog")
                                        directory*)))))

(define* (unpack #:key racket build-lib unpack-plan
                 #:allow-other-keys)
  (invoke racket
          (string-append build-lib "/unpack.rkt")
          "--to" (source-dir)
          "--names-to" (pkg-names.rktd)
          (format #f "~s" unpack-plan))
  (chdir (match (all-pkg-names)
           ((name)
            (string-append (source-dir) "/" name))
           (_
            (source-dir)))))

(define* (find-lib-search-dirs #:key inputs (lib-search-dirs '())
                               build-type
                               #:allow-other-keys)
  (define dest
    (foreign-libs-dir))
  (union-build dest
               (match inputs
                 (((_ . directory*) ...)
                  (search-path-as-list '("lib/racket/guix/foreign-libs")
                                       directory*)))
               ;; well, there's only one directory, but we want to create it
               #:create-all-directories? #t)
  ;(format #t "lib-search-dirs:\n  ~s\n" lib-search-dirs)
  (unless (null? lib-search-dirs)
    (when (eq? 'cycle-member build-type)
      (error "oops"))
    (call-with-output-file (string-append dest "/" (this-package-name))
      (lambda (out)
        (for-each (lambda (dir)
                    (write dir out)
                    (newline out))
                  lib-search-dirs)))))

(define* (link-lib-search-dirs #:key racket config-dir #:allow-other-keys)
  (define user-lib-dir
    (let ((pipe (open-pipe* OPEN_READ
                            racket
                            "--config" config-dir
                            "-e" (format #f "~s"
                                         '(begin
                                            (require setup/dirs)
                                            (write (path->string
                                                    (find-user-lib-dir))))))))
      (let ((pth (read pipe)))
        (close-pipe pipe)
        pth)))
  (define dirs-to-link
    (with-directory-excursion (foreign-libs-dir)
      (append-map (lambda (file)
                    (call-with-input-file file
                      (lambda (in)
                        (let loop ()
                          (match (read in)
                            ((? eof-object?)
                             '())
                            ((? string? dir)
                             (cons dir (loop))))))))
                  (directory-list "."))))
  (mkdir-p (dirname user-lib-dir))
  ;; We should always have at least <openssl>/lib and <sqlite>/lib,
  ;; so no need to #:create-all-directories? here to be able to add more.
  (union-build user-lib-dir dirs-to-link))

(define* (build #:key racket config-dir inputs (parallel-build? #t)
                #:allow-other-keys)
  (define package-sources
    (map (cute string-append (source-dir) "/" <>)
         (all-pkg-names)))
  ;; TOOO: script for more control of behavior on setup error
  (apply invoke
         racket
         "--config" config-dir
         "-l-"
         "raco"
         "pkg"
         "install"
         "--batch"
         "--auto" ;; ? or do we assume parent layer has them ?
         "-j" (if parallel-build?
                  (number->string (parallel-job-count))
                  "1")
         "--catalog" (inputs-catalog)
         "--pkgs"
          package-sources))

(define* (install-raco-test #:key racket config-dir inputs native-inputs target
                            (tests? (not target))
                            (parallel-tests? #t)
                            (parallel-build? #t)
                            #:allow-other-keys)
  (match tests?
    (#f
     (format #t "not installing 'raco test'~%"))
    ('this-package-provides-raco-test
     (format #t "this package provides 'raco test'~%"))
    (_
     (invoke racket
             "--config" config-dir
             "-l-"
             "raco"
             "pkg"
             "install"
             "--batch"
             "--auto"
             "-j" (if parallel-build?
                      (number->string (parallel-job-count))
                      "1")
             "--catalog" (search-input-directory (or native-inputs inputs)
                                                 "/raco-test-catalog")
             "--skip-installed"
             "compiler-lib"))))

(define* (check #:key racket config-dir inputs native-inputs target
                (tests? (not target))
                (parallel-tests? #t)
                (parallel-build? #t)
                #:allow-other-keys)
  (cond
   (tests?
    (apply invoke
           racket
           "--config" config-dir
           "-l-" "raco"
           "test"
           #;"--drdr" ; ???
           "-j" (if parallel-tests?
                    (number->string (parallel-job-count))
                    "1")
           "--heartbeat"
           "--package"
           (if (eq? 'this-package-provides-raco-test tests?)
               `("--deps" ,(this-package-name))
               (all-pkg-names))))
   (else
    (format #t "test suite not run~%"))))

(define* (propagate-input-catalogs #:key outputs #:allow-other-keys)
  (cond
   ((assoc-ref outputs "out")
    => (lambda (prefix)
         (define base
           (string-append prefix "/lib/racket/guix"))
         (mkdir-p base)
         (copy-recursively (string-append (PLTUSERHOME) "/inputs-catalog")
                           (string-append base "/catalog"))))
   (else
    (error "no output \"out\""))))

(define* (propagate-lib-search-dirs #:key outputs #:allow-other-keys)
  (cond
   ((assoc-ref outputs "out")
    => (lambda (prefix)
         (with-directory-excursion (foreign-libs-dir)
           (define dest
             (string-append prefix "/lib/racket/guix/foreign-libs"))
           ;; copy-recursively #:follow-symlinks? #f ??????
           (mkdir-p dest)
           (for-each (lambda (file)
                       (if (symbolic-link? file)
                           (symlink (readlink file)
                                    (string-append dest "/" file))
                           (install-file file dest)))
                     (directory-list ".")))))
   (else
    (error "no output \"out\""))))

(define (mark-selected-package prefix name)
  (define selected-dir
    (string-append prefix "/lib/racket/guix/selected"))
  (mkdir-p selected-dir)
  (with-output-to-file (string-append selected-dir "/" name)
    (lambda ()
      ;; we just want to touch it
      *unspecified*)))

(define* (install #:key outputs racket config-dir build-lib
                  #:allow-other-keys)
  (cond
   ((assoc-ref outputs "out")
    => (lambda (prefix)
         (define pkgs-prefix
           (or (assoc-ref outputs "pkgs")
               prefix))
         (invoke racket
                 "--config" config-dir
                 (string-append build-lib "/export-built.rkt")
                 (string-append pkgs-prefix "/lib/racket/guix/pkgs")
                 (pkg-names.rktd))
         (mark-selected-package prefix (this-package-name))
         ;; We want absolute paths in the catalog, but pkg/dirs-catalog
         ;; produces relative paths. As a work-around, create a temporary
         ;; catalog and move it: `catalog-copy` uses absolute paths.
         (define pre-inst-catalog
           (string-append (PLTUSERHOME) "/pre-inst-catalog"))
         (invoke racket "-l-" "pkg/dirs-catalog"
                 "--merge"
                 "--link"
                 pre-inst-catalog
                 (string-append pkgs-prefix "/lib/racket/guix/pkgs"))
         (catalog-copy #:racket racket
                       #:to (string-append prefix "/lib/racket/guix/catalog")
                       #:merge? #t
                       #:from pre-inst-catalog)))
   (else
    (error "no output \"out\""))))

(define* (check-package-in-cycle #:key inputs cycle-member
                                 #:allow-other-keys)
  (define base "/lib/racket/guix")
  (search-input-directory inputs
                          (string-append base "/pkgs/" cycle-member))
  (search-input-file inputs
                     (string-append base "/catalog/pkg/" cycle-member)))

(define* (install-from-cycle #:key outputs cycle-member #:allow-other-keys)
  (cond
   ((assoc-ref outputs "out")
    => (lambda (prefix)
         (mark-selected-package prefix cycle-member)))
   (else
    (error "no output \"out\""))))


(define* (configure-layer #:key outputs
                          racket config-dir build-lib lib-search-dirs
                          tethered?
                          #:allow-other-keys)
  (invoke racket
          (string-append build-lib "/configure-layer.rkt")
          "--prefix" (or (assoc-ref outputs "out")
                         (error "no output \"out\""))
          "--parent-config-dir" config-dir
          (if tethered?
              "--tethered"
              "--untethered")
          "--foreign-libs-index" (foreign-libs-dir)
          "--extra-lib-search-dirs" (format #f "~s" lib-search-dirs)))

(define* (setup-layer #:key racket outputs (parallel-build? #t)
                      #:allow-other-keys)
  (invoke racket
          "--config" (search-input-directory outputs "/etc/racket")
          "-l-"
          "raco"
          "setup"
          "-j" (if parallel-build?
                   (number->string (parallel-job-count))
                   "1")
          "--no-user"))


(define* (install-into-layer #:key racket outputs
                             packages
                             #:allow-other-keys)
  (apply invoke
         racket
         "--config" (search-input-directory outputs "/etc/racket")
         "-l-"
         "raco" "pkg" "install"
         "--installation"
         "--auto"
         "--batch"
         "--catalog" (inputs-catalog)
         "--pkgs"
         packages))

(define %standard-phases
  (modify-phases gnu:%standard-phases
    ;; keep: set-SOURCE-DATE-EPOCH set-paths install-locale
    (add-after 'install-locale 'set-PLTUSERHOME set-PLTUSERHOME)
    (add-after 'set-PLTUSERHOME 'copy-input-catalogs copy-input-catalogs)
    (replace 'unpack unpack)
    (add-after 'unpack 'find-lib-search-dirs find-lib-search-dirs)
    (add-after 'find-lib-search-dirs 'link-lib-search-dirs link-lib-search-dirs)
    (delete 'bootstrap)
    (delete 'patch-usr-bin-file)
    (delete 'patch-source-shebangs) ;; ???
    (delete 'configure)
    (delete 'patch-generated-file-shebangs) ;; ???
    (replace 'build build)
    (add-before 'check 'install-raco-test install-raco-test)
    (replace 'check check)
    (add-before 'install 'propagate-input-catalogs propagate-input-catalogs)
    (add-after 'propagate-input-catalogs 'propagate-lib-search-dirs
      propagate-lib-search-dirs)
    (replace 'install install)
    (delete 'patch-shebangs) ;; ???
    (delete 'strip) ;; ???
    ;; keep: validate-runpath
    (delete 'validate-documentation-location)
    ;; keep: delete-info-dir-file
    (delete 'patch-dot-desktop-files)
    ;; keep: make-dynamic-linker-cache install-license-files reset-gzip-timestamps
    (delete 'compress-documentation)))

(define %cycle-member-phases
  (modify-phases %standard-phases
    (delete 'unpack)
    (delete 'link-lib-search-dirs)
    (delete 'build)
    (delete 'install-raco-test)
    (replace 'check check-package-in-cycle)
    (replace 'install install-from-cycle)))

(define %installation-layer-phases
  (modify-phases %cycle-member-phases
    (add-before 'check 'configure configure-layer)
    (add-after 'configure 'setup setup-layer)
    (delete 'check)
    (delete 'propagate-input-catalogs)
    (delete 'propagate-lib-search-dirs)
    (replace 'install install-into-layer)))


(define* (racket-build #:key native-inputs inputs (phases %standard-phases)
                       #:allow-other-keys
                       #:rest args)
  "Builds Racket packages, applying all of the PHASES in order."
  (apply gnu:gnu-build
         #:inputs inputs
         #:phases phases
         #:racket (or (false-if-exception
                       (search-input-file (or native-inputs inputs)
                                          "/opt/racket-vm/bin/racket"))
                      (search-input-file (or native-inputs inputs)
                                         "/bin/racket"))
         #:config-dir (dirname
                       (or (false-if-exception
                            (search-input-file (or native-inputs inputs)
                                               "/etc/racket/config.rktd"))
                           (search-input-file (or native-inputs inputs)
                                              "/opt/racket-vm/etc/config.rktd")))
         #:build-lib (search-input-directory (or native-inputs inputs)
                                             "/guix-racket/build")
         args))
