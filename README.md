guix-racket-experiment
======================

Explorations toward improving support for Racket on GNU Guix,
currently focused on implementing a `racket-build-system` and
adding `guix import racket`.

Suggestions welcome!

Getting Started
---------------

To use this repository as a Guix
[channel](https://guix.gnu.org/manual/devel/en/html_node/Channels.html),
create a file containing something like this:

```scheme
(cons (channel
        (name 'guix-racket-experiment)
        (url "https://gitlab.com/philip1/guix-racket-experiment.git")
        (branch "main")
        (introduction
         (make-channel-introduction
          "ef0580fceb8d88453724b2a2aa6fc9631612033c"
          (openpgp-fingerprint
           "F465 ABAC D637 AEAC 1415  55CF CA03 638D FA3F 1C7A"))))
      %default-channels)
```

See also the file [channels](./channels). You might want to use it to
do something like `guix time-machine -C channels -- describe`
or `guix time-machine -C channels -- show racket-compiler-lib`.
