;; SPDX-License-Identifier: GPL-3.0-or-later
;; SPDX-FileCopyrightText: © 2022-2023 Philip McGrath <philip@philipmcgrath.com>

(define-module (wip-gnu packages racket)
  #:use-module (gnu packages)
  #:use-module (gnu packages chez)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages racket)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages tls)
  #:use-module (guix build-system trivial)
  #:use-module (guix diagnostics)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix i18n)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix memoization)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-34)
  #:use-module (wip-guix build-system racket)
  #:export (racket-release-origin ; for (wip-guix import racket)
            %racket-version))

(define %racket-version
  (package-version racket-vm-cs))
(define* (racket-release-origin org-or-repo repo-or-sha256
                                #:optional sha256-if-org
                                #:key
                                (v %racket-version)
                                (commit (string-append "v" v)))
  "Returns an origin for a Git repository managed through the Racket release
process."
  (define-values (org repo hash)
    (if sha256-if-org
        (values org-or-repo repo-or-sha256 sha256-if-org)
        (values "racket" org-or-repo repo-or-sha256)))
  (origin
    (method git-fetch)
    (uri (git-reference
          (url (format #f "https://github.com/~a/~a" org repo))
          (commit commit)))
    (sha256 hash)
    (file-name (git-file-name
                (string-downcase
                 (string-append (if (equal? "racket" org)
                                    ""
                                    "racket-")
                                org "-" repo))
                ;; Why not "version"? Most repositories have multiple
                ;; packages, each with its own "version".
                (if (string-prefix? "v" commit)
                    commit
                    (string-take commit (min 7 (string-length commit))))))))

(define-public racket-minimal-next
  (package
    (inherit (make-racket-installation
              #:name "racket-minimal-next"
              #:packages (delay (list racket-lib))
              #:tethered? #t))
    (synopsis "Racket without bundled packages such as DrRacket")
    (description
     "Racket is a general-purpose programming language in the Scheme family,
with a large set of libraries and a compiler based on Chez Scheme.  Racket is
also a platform for language-oriented programming, from small domain-specific
languages to complete language implementations.

The ``minimal Racket'' distribution includes just enough of Racket for you to
use @command{raco pkg} to install more.  Bundled packages, such as the
DrRacket IDE, are not included.")
    (license (list license:asl2.0 license:expat))))

(define-public racket-with-extras
  (package
    (inherit (make-racket-installation
              #:name "racket-with-extras"
              #:racket racket-minimal-next
              #:packages (delay (list racket-compiler-lib
                                      racket-draw-lib
                                      racket-scribble-text-lib
                                      racket-sandbox-lib
                                      racket-guix-racket-build-lib
                                      racket-guix-racket-import-lib))))
    (synopsis "Racket with some packages")
    (description
     "This package is a Racket installation with the other leaf packages
using @code{racket-build-system} so far.")
    (license (list license:asl2.0 license:expat))))


(define-public racket-lib
  (package
    (name "racket-lib")
    (version %racket-version)
    (properties '((upstream-name . "racket-lib")))
    (source #f)
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs
     (list openssl
           sqlite))
    (arguments
     (list
      #:path `(racket "pkgs/racket-lib")
      #:cyclic-sources `((racket "pkgs/base"))
      #:tests? #f ;; dependency of `raco test`
      #:foreign-libs '("openssl"
                       "sqlite")))
    (home-page "https://pkgs.racket-lang.org/package/racket-lib")
    (synopsis "Native libraries used by base Racket")
    (description "Combines platform-specific native libraries that are useful for base Racket.")
    (license (list license:asl2.0 license:expat))))

(define-public racket-base
  (package
    (inherit (racket-package-from-cycle "base" %racket-version
                                        racket-lib))
    (synopsis "Racket libraries that are currently always available")
    (description "")
    (license (list license:asl2.0 license:expat))))

(define %guix-racket-module-rx-str "\\.rkt$")
(define guix-racket-file?
  (let* ((file-rx (make-regexp %guix-racket-module-rx-str))
         (guix-racket-file?
          (lambda (path stat)
          (if (eq? 'directory (stat:type stat))
              (not (equal? "compiled" (basename path)))
              (regexp-exec file-rx (basename path))))))
    guix-racket-file?))

(define-public racket-guix-racket-build-lib
  (package
    (name "racket-guix-racket-build-lib")
    (version "0.0")
    (source (local-file
             "aux-files/racket/guix-racket-build-lib"
             #:recursive? #t
             #:select? guix-racket-file?))
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs
     (list racket-base))
    (arguments
     (list
      #:path `(#f "guix-racket-build-lib")))
    (home-page "https://pkgs.racket-lang.org/package/guix-racket-build-lib")
    (synopsis "Auxiliary Racket code for @code{racket-build-system}")
    (description "")
    (license (list license:asl2.0 license:expat))))

(define-public racket-guix-racket-import-lib
  (package
    (name "racket-guix-racket-import-lib")
    (version "0.0")
    (properties `((upstream-name . "guix-racket-import-lib")))
    (source (local-file
             "aux-files/racket/guix-racket-import-lib"
             #:recursive? #t
             #:select? guix-racket-file?))
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs
     (list racket-base))
    (arguments
     (list
      #:path `(#f "guix-racket-import-lib")))
    (home-page
     "https://pkgs.racket-lang.org/package/guix-racket-import-lib")
    (synopsis "Auxiliary Racket code for @command{guix import racket}")
    (description "")
    (license (list license:asl2.0 license:expat))))

(define (make-bootstrap-guix-racket pkg racket-vm)
  (hidden-package
   (package
     (inherit pkg)
     (name (string-append "bootstrap-" (guix-package->racket-name pkg)))
     (build-system trivial-build-system)
     (outputs '("out"))
     (inputs '())
     (native-inputs (list racket-vm))
     (arguments
      (list
       #:builder
       (with-imported-modules `((guix build utils))
         #~(begin
             (use-modules (guix build utils))
             (define dest
               (string-append #$output "/guix-racket"))
             (mkdir-p dest)
             (copy-recursively #$(package-source this-package) dest)
             (with-directory-excursion dest
               ;; we don't even have 'raco make' yet
               (invoke
                #$(file-append racket-vm "/opt/racket-vm/bin/racket")
                "-e"
                (format
                 #f "~s"
                 `(begin
                    (require setup/parallel-build)
                    (parallel-compile-files
                     (list ,@(find-files "." #$%guix-racket-module-rx-str))
                     #:handler (lambda args (eprintf "~e\n" args))
                     #:worker-count ,(parallel-job-count)))))))))))))

(define-public bootstrap-guix-racket-import-lib
  (make-bootstrap-guix-racket racket-guix-racket-import-lib racket-vm-cs))

(define-public make-bootstrap-guix-racket-build-lib
  (mlambdaq (racket-vm)
    "Return a version of 'racket-guix-racket-build-lib' for use by
'racket-build-system' with RACKET-VM.

We can't build this code as a normal Racket package because we need it
to build even the 'base' and 'racket-lib' packages."
    (make-bootstrap-guix-racket racket-guix-racket-build-lib racket-vm-cs)))


(define-public racket-at-exp-lib
  (package
    (name "racket-at-exp-lib")
    (version %racket-version)
    (source #f)
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs (list racket-base))
    (arguments
     (list
      #:path `(racket "pkgs/at-exp-lib")))
    (home-page "https://pkgs.racket-lang.org/package/at-exp-lib")
    (synopsis "")
    (description "")
    (license (list license:asl2.0 license:expat))))

(define-public racket-compiler-lib
  (package
    (name "racket-compiler-lib")
    (version %racket-version)
    (source #f)
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs
     (list racket-rackunit-lib
           racket-scheme-lib
           racket-zo-lib
           racket-base))
    (arguments
     (list
      #:path `(racket "pkgs/compiler-lib")
      #:tests? #f #;'this-package-provides-raco-test)) ; <-- FIXME!
    ;; https://github.com/racket/racket/issues/4458
    (home-page "https://pkgs.racket-lang.org/package/compiler-lib")
    (synopsis "")
    (description "")
    (license (list license:asl2.0 license:expat))))

(define-public raco-test-catalog
  ;; this should probably be a function and respond to a modified compiler-lib
  (hidden-package
   (package
     (name "raco-test-catalog")
     (version %racket-version)
     (source #f)
     (build-system trivial-build-system)
     (inputs (list racket-compiler-lib))
     (arguments
      (list
       #:builder
       (with-imported-modules `((guix build utils))
         #~(begin
             (use-modules (guix build utils))
             (copy-recursively
              (string-append #$(this-package-input "racket-compiler-lib")
                             "/lib/racket/guix/catalog")
              (string-append #$output "/raco-test-catalog")
              #:follow-symlinks? #t)))))
     (home-page "https://docs.racket-lang.org/raco/test.html")
     (synopsis "")
     (description "")
     (license (list license:asl2.0 license:expat)))))

(define-public racket-zo-lib
  (package
    (name "racket-zo-lib")
    (version %racket-version)
    (source #f)
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs (list racket-base))
    (arguments
     (list
      #:tests? #f ;; dependency of `raco test`
      #:path `(racket "pkgs/zo-lib")))
    (home-page "https://pkgs.racket-lang.org/package/zo-lib")
    (synopsis "")
    (description "")
    (license (list license:asl2.0 license:expat))))

(define-public racket-rackunit-lib
  (package
    (name "racket-rackunit-lib")
    (version %racket-version)
    (source
     (racket-release-origin
      "rackunit"
      (base32 "1gpz9sgnm8hrc0cb3rii0wzbcwp9mgy5k1amnxidy7gyzl7prn81")))
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs
     (list racket-base
           racket-testing-util-lib))
    (arguments
     (list
      #:tests? #f ;; dependency of `raco test`
      #:path "rackunit-lib"))
    (home-page "https://pkgs.racket-lang.org/package/rackunit-lib")
    (synopsis "")
    (description "")
    (license (list license:asl2.0 license:expat))))

(define-public racket-testing-util-lib
  (package
    (name "racket-testing-util-lib")
    (version %racket-version)
    (source (package-source racket-rackunit-lib))
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs (list racket-base))
    (arguments
     (list
      #:tests? #f ;; dependency of `raco test`
      #:path "testing-util-lib"))
    (home-page "https://pkgs.racket-lang.org/package/testing-util-lib")
    (synopsis "")
    (description "")
    (license (list license:asl2.0 license:expat))))

(define-public racket-scheme-lib
  (package
    (name "racket-scheme-lib")
    (version %racket-version)
    (source
     (racket-release-origin
      "scheme-lib"
      (base32 "0pcf0y8rp4qyjhaz5ww5sr5diq0wpcdfrrnask7zapyklzx1jx8x")))
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs (list racket-base))
    (arguments
     (list
      #:tests? #f ;; dependency of `raco test`
      #:path `(#f "scheme-lib")))
    (home-page "https://pkgs.racket-lang.org/package/scheme-lib")
    (synopsis "Legacy (Scheme) libraries")
    (description "")
    (license (list license:asl2.0 license:expat))))

(define-public racket-scribble-text-lib
  (package
    (name "racket-scribble-text-lib")
    (version %racket-version)
    (source
     (racket-release-origin
      "scribble"
      (base32 "1m3s7nz4qk71hnl2qhnm4fbk4mfz1z53ig21czhinbxpall8l4d1")))
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs (list racket-base
                  racket-at-exp-lib
                  racket-scheme-lib))
    (arguments
     (list
      #:path "scribble-text-lib"))
    (home-page "https://pkgs.racket-lang.org/package/scribble-text-lib")
    (synopsis "Language for text with embedded Racket code")
    (description "")
    (license (list license:asl2.0 license:expat))))

(define-public racket-draw-lib
  (package
    (name "racket-draw-lib")
    (version %racket-version)
    (source
     (racket-release-origin
      "draw"
      (base32 "0p69yk1c1jad5xmr8xxbxvrmq5yb3cr5zlj1kydx3nd0ij3g5dir")))
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs
     (list racket-base
           ;; https://docs.racket-lang.org/draw/libs.html
           cairo
           fontconfig
           glib
           glu ;; is this really needed?
           libjpeg-turbo
           libpng
           pango))
    (arguments
     (list
      #:path "draw-lib"
      #:foreign-libs '("cairo"
                       "fontconfig-minimal" ;; aka fontconfig
                       "glib"
                       "glu"
                       "libjpeg-turbo"
                       "libpng"
                       "pango")))
    (home-page "https://pkgs.racket-lang.org/package/draw-lib")
    (synopsis "Implementation (no documentation) part of @code{racket-draw}")
    (description "")
    (license (list license:asl2.0 license:expat))))

(define-public racket-source-syntax
  (package
    (name "racket-source-syntax")
    (version "1.1")
    (source
     (racket-release-origin
      "typed-racket"
      (base32 "1hhc0nss68814xsxhl5rnw4smnm06573j6ka0wp77almqg5nzhpv")))
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs (list racket-base))
    (arguments
     (list #:path "source-syntax"))
    (home-page "https://pkgs.racket-lang.org/package/source-syntax")
    (synopsis "find mappings from expanded to source syntax")
    (description "find mappings from expanded to source syntax")
    (license (list license:asl2.0 license:expat))))

(define-public racket-errortrace-lib
  (package
    (name "racket-errortrace-lib")
    (version "1.5")
    (source (racket-release-origin
             "errortrace"
             (base32 "0l9k2rx6z4jkid7kxhbs763s66pxbzvfrgxajykbrmqkv77lhb3w")))
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs (list racket-base
                  racket-source-syntax))
    (arguments
     (list #:path "errortrace-lib"))
    (home-page "https://pkgs.racket-lang.org/package/errortrace-lib")
    (synopsis "implementation (no documentation) part of \"errortrace\"")
    (description "implementation (no documentation) part of \"errortrace\"")
    (license (list license:asl2.0 license:expat))))

(define-public racket-sandbox-lib
  (package
    (name "racket-sandbox-lib")
    (version "1.2")
    (source #f)
    (build-system racket-build-system)
    (outputs '("out" "pkgs"))
    (inputs (list racket-base
                  racket-errortrace-lib
                  racket-scheme-lib))
    (arguments
     (list #:path `(racket "pkgs/sandbox-lib")))
    (home-page "https://pkgs.racket-lang.org/package/sandbox-lib")
    (synopsis "Library for sandboxing Racket programs")
    (description "Library for sandboxing Racket programs")
    (license (list license:asl2.0 license:expat))))
